import { Injectable } from '@angular/core';
import {ApiService} from './services//api.service'

@Injectable({
  providedIn: 'root'
})
export class FacadeService {

  constructor(private API: ApiService) { }

  getPokemons(limit){    
    return this.API.getPokemons(limit).then((results) => {
      return Promise.all(results).then((response) => {
        return response
      })
    });
  }

  getPokemonsByType(url){
    return this.API.getPokemonsByTypes(url).then((results) => {
      return Promise.all(results).then((response) => {
         return response
      })
    });
  }

  async getTypes(){
    return await this.API.getTypes();
  }

}


