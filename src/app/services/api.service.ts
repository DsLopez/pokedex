import { Injectable } from '@angular/core';

const BASE_API = "https://pokeapi.co/api/v2/";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor() { }

  async getPokemons(limit){
    const query = await fetch(`${BASE_API}pokemon?limit=${limit}`)
    const {results} = await query.json();
    var arry = results.map ( async (pokemon) =>{
      let { url } = pokemon;
      let queryPokemon = await fetch(url);
      let resultPokemon = await queryPokemon.json()
      return resultPokemon
    })
    return arry

  }

  async getPokemonsByTypes(url){
    const query = await fetch(url)    
    const {pokemon} = await query.json();
    const pokefilt = pokemon.splice(0,20)   
    var arry = pokefilt.map ( async (poke) =>{
      let queryPokemon = await fetch(poke.pokemon.url);
      let resultPokemon = await queryPokemon.json()
      return resultPokemon
    })
    return arry
  }

  async getTypes(){
    const query = await fetch(`${BASE_API}type`)
    const { results } = await query.json();
    return results
  }




}
