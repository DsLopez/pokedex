import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

import { ChartDataSets, ChartType, RadialChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import {FacadeService} from '../../facade.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  pokemons: any = [];
  typesPokemon: any = [];
  filterPokemons: any[];
  pokemon: any = {};
  detailPoke = false;
  searchPoke: string;
  limit: number = 20;
  search: string = '';
  xpandStatus = false

  public radarChartOptions: RadialChartOptions = {
    responsive: true,
  };
  public radarChartType: ChartType = 'radar';
  public radarChartLabels: Label[] = ['Velocidad', 'Defensa Especial', 'Ataque especial', 'Defensa', 'Ataque', 'Vida'];
  public radarChartData: ChartDataSets[] = [
    { data: [], label: 'Stats' }
  ];
  constructor(private apiService: ApiService, private facade: FacadeService) { }


  findPokemoByType(type){
    this.xpandStatus = false
    const { url }= type;

    //Patron estructural Facade
    this.facade.getPokemonsByType(url).then((result)=>{
      this.pokemons = result;
      this.filterPokemons = this.pokemons
    })
  }

  viewMorePokemons(){
    this.limit+=8;
    this.getPokemons();
  }

  searchPokemon(event) {
    this.filterPokemons = this.pokemons.filter((pokemon) => {
      return pokemon.name.toLowerCase().indexOf(event) === 0
    })
  }

  getPokemons() {
    //Patron estructural Facade
    this.facade.getPokemons(this.limit).then((result)=>{
      this.pokemons = result;
      this.filterPokemons = this.pokemons
    })

  }

  async getTypes(){
    this.typesPokemon = await this.facade.getTypes();     
  }

  detailPokemon(pokemon) {
    this.detailPoke = true;
    this.pokemon = pokemon;
    let { stats } = pokemon
    this.addDataStast(stats)
  }

  addDataStast(stats) {
    this.radarChartData[0].data = []
    stats.map((stats) => {
      this.radarChartData[0].data.push(stats.base_stat)
    })
  }

  goBack() {
    this.detailPoke = false;
    this.pokemon = {}
  }

  ngOnInit() {
    this.getPokemons();
    this.getTypes();
  }



}
